# Copyright 2008, 2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A C library that performs DNS requests and name resolves asynchronously"
DESCRIPTION="
c-ares is a C library that performs DNS requests and name resolves asynchronously.
c-ares is a fork of the library named 'ares', written by Greg Hudson at MIT.
"
HOMEPAGE="https://c-ares.org"
DOWNLOADS="${HOMEPAGE}/download/${PNV}.tar.gz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="debug"

# require network
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    test:
        dev-cpp/gtest
"

src_configure() {
    local args=(
        --enable-cares-threads
        --enable-shared
        --disable-static
        --disable-werror
        --with-random=/dev/urandom
        $(option_enable debug)
    )

    if exhost --is-native -q; then
        args+=( $(expecting_tests '--enable-tests' '--disable-tests') )
    fi

    econf "${args[@]}"
}

