# Copyright 2020 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

SUMMARY="Manages llvm's compiler-tools symlinks"
HOMEPAGE="https://www.exherbo.org"
DOWNLOADS=""

LICENCES="GPL-2"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"

SLOT="0"

CROSS_COMPILE_TARGETS="
    aarch64-unknown-linux-musleabi
    aarch64-unknown-linux-gnueabi
    armv7-unknown-linux-gnueabi
    armv7-unknown-linux-gnueabihf
    i686-pc-linux-gnu
    i686-pc-linux-musl
    powerpc64-unknown-linux-gnu
    x86_64-pc-linux-gnu
    x86_64-pc-linux-musl
"

MYOPTIONS="
    ( targets: ${CROSS_COMPILE_TARGETS} ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    run:
        app-admin/eclectic[>=2.0.20]
        dev-lang/llvm:*
"

WORK="${WORKBASE}"

eclectic-llvm_alternatives() {
    local host=$(exhost --target)
    local target=${1}
    local alternatives=()
    local as_alternatives=()
    # Unfortunately not all tools are named equally in binutils and LLVM, e.g. LLVM's demangler is
    # not called llvm-c++filt, but llvm-cxxfilt.
    # To account for this the items in the following array can be in the form
    #   "<alternative name>:<LLVM tool name>"
    #
    # Not supported yet: elfedit gprof
    local compiler_tools=( addr2line ar "c++filt:cxxfilt" dwp elfedit gprof nm objcopy objdump ranlib readelf size strings strip )
    local tool tool_alternative tool_llvm

    for tool in ${compiler_tools[@]}; do
        tool_alternative=${tool%%:*}
        tool_llvm=${tool##*:}

        [[ ! -x /usr/${host}/bin/llvm-${tool_llvm} ]] && continue

        alternatives+=(
            /usr/${host}/bin/${target}-${tool_alternative} llvm-${tool_llvm}
            /usr/${host}/${target}/bin/${tool_alternative} ../../bin/llvm-${tool_llvm}
        )

        if [[ ${host} == ${target} ]]; then
            dobanned ${tool_alternative}
            alternatives+=(
                /usr/${host}/bin/${tool_alternative} llvm-${tool_llvm}
                "${BANNEDDIR}"/${tool_alternative}   ${tool_alternative}.llvm
            )
        fi
    done

    as_alternatives+=(
        /usr/${host}/bin/${target}-as llvm-as
        /usr/${host}/${target}/bin/as ../../bin/llvm-as
    )
    if [[ ${host} == ${target} ]]; then
        dobanned as
        as_alternatives+=(
            /usr/${host}/bin/as llvm-as
            "${BANNEDDIR}"/as   as.llvm
        )
    fi

    alternatives_for compiler-tools llvm 10 "${alternatives[@]}"
    alternatives_for assembler llvm 10 "${as_alternatives[@]}"
}

src_install() {
    for target in ${CROSS_COMPILE_TARGETS};do
        if option targets:${target};then
            eclectic-llvm_alternatives ${target}
        fi
    done
}
