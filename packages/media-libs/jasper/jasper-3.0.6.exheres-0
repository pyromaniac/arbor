# Copyright 2008 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=${PN}-software release=version-${PV} suffix=tar.gz ] cmake

SUMMARY="Software-based implementation of the codec specified in the JPEG-2000 Part-1 standard"
HOMEPAGE+=" https://www.ece.uvic.ca/~frodo/${PN}"

LICENCES="Jasper-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    heif [[ description = [ Support for the H(igh) E(fficiency) I(mage) F(ile) F(ormat) ] ]]
    opengl
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        heif? ( media-libs/libheif )
        opengl? (
            dev-libs/libglvnd
            x11-dri/freeglut
            x11-dri/glu
        )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DCMAKE_DISABLE_FIND_PACKAGE_Doxygen:BOOL=TRUE
    -DCMAKE_INSTALL_DOCDIR=/usr/share/doc/${PNVR}
    -DJAS_ENABLE_32BIT:BOOL=FALSE
    -DJAS_ENABLE_ASAN:BOOL=FALSE
    -DJAS_ENABLE_BMP_CODEC:BOOL=TRUE
    -DJAS_ENABLE_CXX:BOOL=FALSE
    -DJAS_ENABLE_DANGEROUS_INTERNAL_TESTING_MODE:BOOL=FALSE
    -DJAS_ENABLE_DOC:BOOL=FALSE
    -DJAS_ENABLE_HIDDEN:BOOL=TRUE
    -DJAS_ENABLE_FUZZER:BOOL=FALSE
    -DJAS_ENABLE_HIDDEN:BOOL=FALSE
    -DJAS_ENABLE_JP2_CODEC:BOOL=TRUE
    -DJAS_ENABLE_JPC_CODEC:BOOL=TRUE
    -DJAS_ENABLE_JPG_CODEC:BOOL=TRUE
    -DJAS_ENABLE_LATEX:BOOL=FALSE
    -DJAS_ENABLE_LIBJPEG:BOOL=TRUE
    -DJAS_ENABLE_LSAN:BOOL=FALSE
    -DJAS_ENABLE_MIF_CODEC:BOOL=FALSE
    -DJAS_ENABLE_MSAN:BOOL=FALSE
    -DJAS_ENABLE_MULTITHREADING_SUPPORT:BOOL=TRUE
    -DJAS_ENABLE_NON_THREAD_SAFE_DEBUGGING:BOOL=FALSE
    -DJAS_ENABLE_PGX_CODEC:BOOL=TRUE
    -DJAS_ENABLE_PNM_CODEC:BOL=TRUE
    -DJAS_ENABLE_PROGRAMS:BOOL=TRUE
    -DJAS_ENABLE_RAS_CODEC:BOOL=TRUE
    -DJAS_ENABLE_SHARED:BOOL=TRUE
    -DJAS_ENABLE_TSAN:BOOL=FALSE
    -DJAS_ENABLE_UBSAN:BOOL=FALSE
    -DJAS_INCLUDE_BMP_CODEC:BOOL=TRUE
    -DJAS_INCLUDE_JP2_CODEC:BOOL=TRUE
    -DJAS_INCLUDE_JPC_CODEC:BOOL=TRUE
    -DJAS_INCLUDE_JPG_CODEC:BOOL=TRUE
    -DJAS_INCLUDE_MIF_CODEC:BOOL=TRUE
    -DJAS_INCLUDE_PGX_CODEC:BOOL=TRUE
    -DJAS_INCLUDE_PNM_CODEC:BOOL=TRUE
    -DJAS_INCLUDE_RAS_CODEC:BOOL=TRUE
    -DJAS_PREFER_PTHREAD:BOOL=TRUE
    -DJAS_PREFER_PTHREAD_TSS:BOOL=FALSE
    -DJAS_STRICT:BOOL=FALSE
    -DJAS_USE_JAS_INIT:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'heif JAS_ENABLE_HEIC_CODEC'
    'heif JAS_ENABLE_LIBHEIF'
    'heif JAS_INCLUDE_HEIC_CODEC'
    'opengl JAS_ENABLE_OPENGL'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

