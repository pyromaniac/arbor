# Copyright 2009-2014 Wulf C. Krueger
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'libcap-2.08-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require pam flag-o-matic

export_exlib_phases src_prepare src_install

SUMMARY="POSIX 1003.1e capabilities"
HOMEPAGE="https://sites.google.com/site/fullycapable/"
DOWNLOADS="mirror://kernel/linux/libs/security/linux-privs/${PN}${PV:0:1}/${PNV}.tar.xz"

LICENCES="|| ( GPL-2 BSD-3 )"
SLOT="0"
MYOPTIONS="parts: binaries configuration development documentation libraries"

DEPENDENCIES="
    build:
        dev-util/gperf
        sys-kernel/linux-headers
    build+run:
        sys-apps/attr
        sys-libs/pam
"

# Needs sudo to run
RESTRICT="test"

DEFAULT_SRC_COMPILE_PARAMS=(
    BUILD_CC="$(exhost --build)-cc"
    BUILD_CFLAGS="$(print-build-flags CFLAGS)"
    AR=$(exhost --tool-prefix)ar
    CC=$(exhost --tool-prefix)cc
    RANLIB=$(exhost --tool-prefix)ranlib
    OBJCOPY=$(exhost --tool-prefix)objcopy
    prefix=/usr/$(exhost --target)
    lib=lib
    PAM_CAP=yes
    GOLANG=no
)

DEFAULT_SRC_INSTALL_PARAMS=(
    prefix=/usr/$(exhost --target)
    SBINDIR=/usr/$(exhost --target)/bin
    OBJCOPY=$(exhost --tool-prefix)objcopy
    lib=lib
    man_prefix=/usr/share
    RAISE_SETFCAP=no
    GOLANG=no
)

libcap_src_prepare() {
    default

    edo sed -e 's/^\(AR\|CC\|RANLIB\|OBJCOPY\) :=/\1 ?=/' \
            -i Make.Rules
}

libcap_src_install() {
    default

    dopammod pam_cap/pam_cap.so
    dopamsecurity '' pam_cap/capability.conf

    dodoc doc/capability.md

    expart binaries /usr/$(exhost --target)/bin
    expart configuration /etc
    expart development /usr/$(exhost --target)/include
    expart documentation /usr/share/{doc,man}
    expart libraries /usr/$(exhost --target)/lib
}

