# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    SCM_REPOSITORY="https://sourceware.org/git/dwz.git"
    if ! ever at_least scm; then
        SCM_TAG="${PNV%-scm}"
    fi

    require scm-git
else
    DOWNLOADS="mirror://sourceware/${PN}/releases/${PNV}.tar.xz"
fi

require flag-o-matic

export_exlib_phases src_prepare

SUMMARY="DWARF optimization and duplicate removal tool"
DESCRIPTION="
The dwz package contains a program that attempts to optimize DWARF debugging
information contained in ELF shared libraries and ELF executables for size, by
replacing DWARF information representation with equivalent smaller
representation where possible and by reducing the amount of duplication using
techniques from DWARF standard appendix E - creating DW_TAG_partial_unit
compilation units (CUs) for duplicated information and using
DW_TAG_imported_unit to import it into each CU that needs it.
"
HOMEPAGE="https://sourceware.org/dwz/"

MYOPTIONS="
    ( libc: musl )
"

DEPENDENCIES="
    build+run:
        dev-libs/xxHash
        dev-util/elfutils
        libc:musl? ( dev-libs/musl-obstack )
    test:
        dev-util/dejagnu
"

# Fails a lot of tests, probably because of dejagnu
RESTRICT="test"

WORK="${WORKBASE}"/${PN}

dwz_src_prepare() {
    default

    if [[ $(exhost --target) == *-musl* ]];then
        # NOTE(somasis) dwz needs an external obstack library on musl
        edo sed -e 's/-lelf/& -lobstack/' -i Makefile

        # {args,dwz}.c expect to use error() from `<error.h>`, but musl doesn't provide that header
        edo cp "${FILES}"/error.h "${WORK}"/
        edo sed -e 's/<error.h>/"error.h"/' -i {args,dwz}.c

        # Necessary because error.h uses `program_invocation_name`
        append-cppflags -D_GNU_SOURCE
    fi

    edo sed -e "s/shell readelf/shell $(exhost --tool-prefix)readelf/" \
        -i Makefile

    edo sed -e "s/objcopy/$(exhost --tool-prefix)objcopy/" \
        -i testsuite/dwz.tests/objcopy-{eu-unstrip,eu-unstrip-multifile,remove-debug-abbrev,strip-debug}.sh \
        -i testsuite/dwz.tests/pr24174.sh
}

