# Copyright 2007 Bryan Østergaard <kloeri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.xz ] autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
require utf8-locale

SUMMARY="GNU Documentation System"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ( linguas: ca cs da de el eo es fi fr he hr hu id it ja ka nb nl pl pt_BR pt ro ru rw sk sl sr
               sv tr uk vi zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.21]
    build+run:
        dev-lang/perl:=[>=5.8.1]
        dev-libs/libunistring:=
        dev-perl/libintl-perl
        dev-perl/Text-Unidecode
        dev-perl/Unicode-EastAsianWidth
        sys-libs/ncurses
    test:
        dev-perl/Data-Compare   [[ note = [ for --enable-tp-tests ] ]]
        dev-perl/Data-Dump      [[ note = [ for --enable-tp-tests ] ]]
        dev-perl/Test-Deep      [[ note = [ for --enable-tp-tests ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --enable-perl-xs
    --enable-tp-tests
    --with-external-libintl-perl
    --with-external-Text-Unidecode
    --with-external-Unicode-EastAsianWidth
)

AT_M4DIR=( gnulib/m4 )

src_prepare() {
    # disable failing test depending on available locales
    # e.g. http://lists.gnu.org/archive/html/bug-texinfo/2015-12/msg00017.html
    edo sed \
        -e '/test_scripts\/formatting_documentlanguage_set_option.sh\ \\/d' \
        -e '/test_scripts\/coverage_formatting_fr.sh\ \\/d' \
        -e '/test_scripts\/layout_formatting_fr_icons.sh\ \\/d' \
        -i tp/tests/Makefile.onetst

    autotools_src_prepare
}

src_test() {
    # for tp/t/languages.t
    require_utf8_locale

    default
}

