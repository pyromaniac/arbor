# Copyright 2012-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=stachenov tag=v${PV} ] cmake

SUMMARY="Simple C++ wrapper over Gilles Vollant's ZIP/UNZIP package"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: qt5 qt6 ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build+run:
        sys-libs/zlib
        providers:qt5? ( x11-libs/qtbase:5 )
        providers:qt6? (
            x11-libs/qt5compat:6
            x11-libs/qtbase:6
        )
"

_quazip_conf() {
    local cmake_params=(
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -DQUAZIP_ENABLE_TESTS:BOOL=$(expecting_tests TRUE FALSE)
        -DQUAZIP_INSTALL:BOOL=TRUE
        -DQUAZIP_USE_QT_ZLIB:BOOL=FALSE
        "$@"
    )

    ecmake "${cmake_params[@]}"
}

src_configure() {
    if option providers:qt5 ; then
        edo mkdir "${WORKBASE}"/qt5-build
        edo pushd "${WORKBASE}"/qt5-build
        _quazip_conf -DQUAZIP_QT_MAJOR_VERSION:STRING=5
        edo popd
    fi

    if option providers:qt6 ; then
        edo mkdir "${WORKBASE}"/qt6-build
        edo pushd "${WORKBASE}"/qt6-build
        _quazip_conf -DQUAZIP_QT_MAJOR_VERSION:STRING=6
        edo popd
    fi
}

src_compile() {
    if option providers:qt5 ; then
        edo pushd "${WORKBASE}"/qt5-build
        ecmake_build
        edo popd
    fi

    if option providers:qt6 ; then
        edo pushd "${WORKBASE}"/qt6-build
        ecmake_build
        edo popd
    fi
}

src_test() {
    if option providers:qt5 ; then
        edo pushd "${WORKBASE}"/qt5-build
        ectest
        edo popd
    fi

    if option providers:qt6 ; then
        edo pushd "${WORKBASE}"/qt6-build
        ectest
        edo popd
    fi
}

src_install() {
    if option providers:qt5 ; then
        edo pushd "${WORKBASE}"/qt5-build
        ecmake_install
        edo popd
    fi

    if option providers:qt6 ; then
        edo pushd "${WORKBASE}"/qt6-build
        ecmake_install
        edo popd
    fi

    edo pushd "${CMAKE_SOURCE}"
    emagicdocs
    edo popd
}

